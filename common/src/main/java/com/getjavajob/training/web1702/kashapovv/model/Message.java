package com.getjavajob.training.web1702.kashapovv.model;

import com.getjavajob.training.web1702.kashapovv.Entity;

import java.time.LocalDate;

/**
 * Created by Вадим on 20.06.2017.
 */
public class Message implements Entity {
    byte[] image;
    private long id;
    private String text;
    private LocalDate created;
    private Account author;
    private Purpose purpose;

    public Message(String text, LocalDate created, Account author, Purpose purpose) {
        this.text = text;
        this.created = created;
        this.author = author;
        this.purpose = purpose;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public Account getAuthor() {
        return author;
    }

    public byte[] getImage() {
        return image;
    }

    public String getText() {
        return text;
    }

    public LocalDate getCreated() {
        return created;
    }

    public Purpose getPurpose() {
        return purpose;
    }

    public enum Purpose {
        Group, Account, Wall
    }
}
