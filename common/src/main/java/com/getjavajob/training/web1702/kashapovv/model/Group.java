package com.getjavajob.training.web1702.kashapovv.model;

import com.getjavajob.training.web1702.kashapovv.Entity;

import java.util.Set;

/**
 * Created by Вадим on 26.04.2017.
 */
public class Group implements Entity {
    private long id;
    private String name;
    private String description;
    private Category category;
    private Set<Account> groupOwners;
    private Set<Account> groupSubscribers;

    public Group(String name, Category category, String description) {
        this.name = name;
        this.description = description;
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        return getName().equals(group.getName());

    }

    @Override
    public int hashCode() {
        return getName().toLowerCase().hashCode();
    }

    public Set<Account> getGroupOwners() {
        return groupOwners;
    }

    public void setGroupOwners(Set<Account> groupOwners) {
        this.groupOwners = groupOwners;
    }

    public Set<Account> getGroupSubscribers() {
        return groupSubscribers;
    }

    public void setGroupSubscribers(Set<Account> groupSubscribers) {
        this.groupSubscribers = groupSubscribers;
    }

    @Override
    public String toString() {
        return "Group{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", category=" + category +
                '}';
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public enum Category {
        NEWS, SPORT, MUSIC, RADIO_TV, ENTERTAINMENT, BLOG, GAMES, SCIENCE_TECH, FASHION_BEAUTY, CULTURE_ART, CHARITY, BRAND, SHOP;
    }

}
