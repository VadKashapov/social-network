package com.getjavajob.training.web1702.kashapovv.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by Вадим on 11.05.2017.
 */

@Getter
@Setter
@EqualsAndHashCode
@ToString
public class Phone {
    private PhoneType type;
    private String number;

    public Phone(PhoneType type, String number) {
        this.type = type;
        this.number = number;
    }

    public enum PhoneType {
        HOME, JOB
    }
}
