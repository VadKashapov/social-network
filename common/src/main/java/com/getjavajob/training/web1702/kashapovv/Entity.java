package com.getjavajob.training.web1702.kashapovv;

/**
 * Created by Вадим on 26.04.2017.
 */
public interface Entity {
    long getId();

    void setId(long id);
}
