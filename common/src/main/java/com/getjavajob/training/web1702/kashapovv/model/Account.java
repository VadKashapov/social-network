package com.getjavajob.training.web1702.kashapovv.model;

import com.getjavajob.training.web1702.kashapovv.Entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Вадим on 26.04.2017.
 */
public class Account implements Entity {
    private long id;
    private String lastName;
    private String firstName;
    private String middleName;
    private LocalDate birthday;
    private List<Phone> personalPhoneNumbers = new ArrayList<>();
    private List<Phone> jobPhoneNumbers = new ArrayList<>();
    private String address;
    private String email;
    private String icqId;
    private String skypeId;
    private String addInInfo;
    private Collection<Account> friends = new ArrayList<>();
    private Collection<Group> groups = new ArrayList<>();

    public Account(String lastName, String firstName, String middleName, LocalDate birthday
            , List<Phone> personalPhoneNumbers, List<Phone> jobPhoneNumbers, String address, String email, String icqId
            , String skypeId, String addInInfo) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.birthday = birthday;
        this.personalPhoneNumbers = personalPhoneNumbers;
        this.jobPhoneNumbers = jobPhoneNumbers;
        this.address = address;
        this.email = email;
        this.icqId = icqId;
        this.skypeId = skypeId;
        this.addInInfo = addInInfo;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public List<Phone> getPersonalPhoneNumbers() {
        return personalPhoneNumbers;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIcqId() {
        return icqId;
    }

    public void setIcqId(String icqId) {
        this.icqId = icqId;
    }

    public String getSkypeId() {
        return skypeId;
    }

    public void setSkypeId(String skypeId) {
        this.skypeId = skypeId;
    }

    public String getAddInInfo() {
        return addInInfo;
    }

    public void setAddInInfo(String addInInfo) {
        this.addInInfo = addInInfo;
    }

    @Override
    public String toString() {
        return "Account{" +
                " id='" + id + '\'' +
                " lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", birthday=" + birthday +
                ", PhoneNumbers='" + printPhoneNumbers() + '\'' +
                ", address='" + address + '\'' +
                ", email='" + email + '\'' +
                ", icqId='" + icqId + '\'' +
                ", skypeId='" + skypeId + '\'' +
                ", addInInfo='" + addInInfo + '\'' +
                '}';
    }

    private String printPhoneNumbers() {
        StringBuilder builder = new StringBuilder();
        for (Phone phoneNumber : personalPhoneNumbers) {
            builder.append(phoneNumber).append(", ");
        }
        builder.setLength(builder.length() - 2);
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        return getEmail().equals(account.getEmail());
    }

    @Override
    public int hashCode() {
        return getEmail().toLowerCase().hashCode();
    }

    public Collection<Account> getFriends() {
        return friends;
    }

    public void setFriends(Collection<Account> friends) {
        this.friends = friends;
    }

    public Collection<Group> getGroups() {
        return groups;
    }

    public List<Phone> getJobPhoneNumbers() {
        return jobPhoneNumbers;
    }
}
