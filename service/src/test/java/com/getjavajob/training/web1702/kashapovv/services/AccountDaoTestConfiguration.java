package com.getjavajob.training.web1702.kashapovv.services;

import com.getjavajob.training.web1702.kashapovv.dao.AccountDao;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

/**
 * Created by Вадим on 02.02.2018.
 */
@Profile("test")
@Configuration
public class AccountDaoTestConfiguration {
    @Bean
    @Primary
    public AccountDao accountDao() {
        return Mockito.mock(AccountDao.class);
    }
}
