package com.getjavajob.training.web1702.kashapovv.services;

import com.getjavajob.training.web1702.kashapovv.dao.AccountDao;
import com.getjavajob.training.web1702.kashapovv.model.Account;
import com.getjavajob.training.web1702.kashapovv.model.Phone;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

import static com.getjavajob.training.web1702.kashapovv.model.Phone.PhoneType.HOME;
import static com.getjavajob.training.web1702.kashapovv.model.Phone.PhoneType.JOB;
import static org.junit.Assert.assertArrayEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Вадим on 05.05.2017.
 */
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountServiceTest {
    @Autowired
    private AccountDao dao;
    @Autowired
    private AccountService accountService;
    private Account account;
    private Account friend;

    @Before
    public void setUp() throws Exception {
        account = new Account("Большаков", "Даниил", "Максимович"
                , LocalDate.of(1990, 3, 2), Arrays.asList(new Phone(HOME, "8(961)125-77-75")), Arrays.asList(new Phone(JOB, "8(495)123-52-43(89)"))
                , "601833, г. Усть-Хайрюзово, ул. Весенняя, дом 85, квартира 121", "bolshak@mail.ru", null, null, null);
        friend = new Account("Городнов", "Филимон", "Валентинович", LocalDate.of(1965, 8, 4), Arrays.asList(new Phone(HOME, "8(957)874-49-82"))
                , new ArrayList<>(), "663282, г. Славинск, ул. Голландская, дом 5, квартира 128", "gorodfilim@mail.ru", null, null, null);
    }

    @Test
    public void createTest() throws Exception {
        when(dao.insert(account)).thenReturn(account);
        accountService.create(account);
        verify(dao).insert(account);
    }

    @Test
    public void updateTest() throws Exception {
        accountService.update(account);
        verify(dao).update(account);
    }

    @Test
    public void deleteTest() throws Exception {
        accountService.delete(account);
        verify(dao).delete(account);
    }

    @Test
    public void addFriendRequestTest() throws Exception {
        accountService.addFriendRequest(account, friend);
        verify(dao).addFriendRequest(account, friend);
    }

    @Test
    public void removeFriendRequestTest() throws Exception {
        accountService.removeFriendRequest(account, friend);
        verify(dao).removeFriendRequest(account, friend);
    }

    @Test
    public void addFriendTest() throws Exception {
        when(dao.isFriendRequestApplied(account, friend)).thenReturn(true);
        accountService.addFriend(account, friend);
        verify(dao).addFriend(account, friend);
    }

    @Test
    public void removeFriendTest() throws Exception {
        accountService.removeFriend(account, friend);
        verify(dao).removeFriend(account, friend);
    }

    @Test
    public void getFriendsTest() throws Exception {
        when(dao.getAllFriends(account)).thenReturn(Arrays.asList(friend));
        assertArrayEquals(new Object[]{friend}, accountService.getFriends(account).toArray());
        verify(dao).getAllFriends(account);
    }

    @Test
    public void getAllTest() throws Exception {
        when(dao.getAll()).thenReturn(Arrays.asList(account, friend));
        assertArrayEquals(new Object[]{account, friend}, accountService.getAll().toArray());
        verify(dao).getAll();
    }
}