package com.getjavajob.training.web1702.kashapovv.services;

/**
 * Created by Вадим on 19.07.2017.
 */
public class TransactionException extends RuntimeException {
    public TransactionException(String message, Throwable cause) {
        super(message, cause);
    }
}
