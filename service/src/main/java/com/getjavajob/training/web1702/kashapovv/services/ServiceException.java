package com.getjavajob.training.web1702.kashapovv.services;

/**
 * Created by Вадим on 05.05.2017.
 */
public class ServiceException extends RuntimeException {

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
