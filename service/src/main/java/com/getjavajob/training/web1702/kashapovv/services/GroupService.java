package com.getjavajob.training.web1702.kashapovv.services;

import com.getjavajob.training.web1702.kashapovv.dao.AbstractDao;
import com.getjavajob.training.web1702.kashapovv.dao.DaoException;
import com.getjavajob.training.web1702.kashapovv.dao.GroupDao;
import com.getjavajob.training.web1702.kashapovv.model.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.List;

/**
 * Created by Вадим on 15.07.2017.
 */
@org.springframework.stereotype.Service
public class GroupService extends AbstractService<Group> {

    public GroupService() throws DaoException {
    }

    @Autowired
    @Qualifier("groupDao")
    private void setDao(AbstractDao<Group> dao) {
        this.dao = dao;
    }

    public List<Group> findByPartName(String partName) throws ServiceException, TransactionException {
        try {
            return ((GroupDao) dao).findByPartName(partName);
        } catch (DaoException e) {
            throw new ServiceException("ERROR - Failed to find groups by partial name", e);
        }
    }
}
