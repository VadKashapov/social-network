package com.getjavajob.training.web1702.kashapovv.services;

import com.getjavajob.training.web1702.kashapovv.dao.AbstractDao;
import com.getjavajob.training.web1702.kashapovv.dao.AccountDao;
import com.getjavajob.training.web1702.kashapovv.dao.DaoException;
import com.getjavajob.training.web1702.kashapovv.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Вадим on 03.05.2017.
 */
@org.springframework.stereotype.Service
public class AccountService extends AbstractService<Account> {

    public AccountService() throws DaoException {
    }

    @Autowired
    @Qualifier("accountDao")
    private void setDao(AbstractDao<Account> dao) {
        this.dao = dao;
    }

    public boolean isAuthorized(String email, byte[] passwordHash) throws ServiceException, TransactionException {
        try {
            return ((AccountDao) dao).isAuthorized(email, passwordHash); //todo Проверить отключен ли автокоммит
        } catch (DaoException e) {
            throw new ServiceException("ERROR - Failed to authorize user", e);
        }
    }

    public Account findByEmail(String email) throws ServiceException, TransactionException {
        try {
            return ((AccountDao) dao).getByEmail(email);
        } catch (DaoException e) {
            throw new ServiceException("ERROR - Failed to get account by Email", e);
        }
    }

    @Transactional
    public boolean addFriendRequest(Account account, Account friend) throws ServiceException, TransactionException {
        try {
            return ((AccountDao) dao).addFriendRequest(account, friend);
        } catch (DaoException e) {
            throw new ServiceException("ERROR - Failed to add friend request", e);
        }
    }

    @Transactional
    public boolean removeFriendRequest(Account account, Account friend) throws ServiceException, TransactionException {
        try {
            return ((AccountDao) dao).removeFriendRequest(account, friend);
        } catch (DaoException e) {
            throw new ServiceException("ERROR - Failed to remove friend request", e);
        }
    }

    @Transactional
    public boolean addFriend(Account account, Account friend) throws ServiceException, TransactionException {
        try {
            AccountDao accountDao = (AccountDao) this.dao;
            if (accountDao.isFriendRequestApplied(account, friend)) {
                boolean result = accountDao.addFriend(account, friend);
                accountDao.removeFriendRequest(account, friend);
                accountDao.removeFriendRequest(friend, account);
                return result;
            }
        } catch (DaoException e) {
            throw new ServiceException("ERROR - Failed to add friend to account", e);
        }
        return false;
    }

    @Transactional
    public boolean removeFriend(Account account, Account friend) throws ServiceException, TransactionException {
        try {
            return ((AccountDao) dao).removeFriend(account, friend);
        } catch (DaoException e) {
            throw new ServiceException("ERROR - Failed to remove friend from account", e);
        }
    }

    public List<Account> getFriends(Account account) throws ServiceException, TransactionException {
        try {
            return ((AccountDao) dao).getAllFriends(account);
        } catch (DaoException e) {
            throw new ServiceException("ERROR - Failed to get friend list from account", e);
        }
    }

    @Transactional
    public void createPassword(String email, byte[] password) throws ServiceException, TransactionException {
        try {
            ((AccountDao) dao).insertPassword(email, password);
        } catch (DaoException e) {
            throw new ServiceException("ERROR - Failed to create password", e);
        }
    }

    @Transactional
    public void updatePassword(String email, byte[] password) throws ServiceException, TransactionException {
        try {
            ((AccountDao) dao).updatePassword(email, password);
        } catch (DaoException e) {
            throw new ServiceException("ERROR - Failed to update password", e);
        }
    }

    public List<Account> findByPartName(String partName) throws ServiceException, TransactionException {
        try {
            return ((AccountDao) dao).findByPartName(partName);
        } catch (DaoException e) {
            throw new ServiceException("ERROR - Failed to find accounts by partial name", e);
        }
    }
}