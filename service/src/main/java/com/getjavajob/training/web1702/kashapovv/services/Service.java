package com.getjavajob.training.web1702.kashapovv.services;

import com.getjavajob.training.web1702.kashapovv.Entity;

import java.util.List;

/**
 * Created by Вадим on 16.07.2017.
 */
public interface Service<E extends Entity> {
    E create(E entity) throws ServiceException, TransactionException;

    void update(E entity) throws ServiceException, TransactionException;

    void delete(E entity) throws ServiceException, TransactionException;

    List<E> getAll() throws ServiceException, TransactionException;

    E getById(long id) throws ServiceException, TransactionException;
}
