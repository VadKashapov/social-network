package com.getjavajob.training.web1702.kashapovv.services;

import com.getjavajob.training.web1702.kashapovv.Entity;
import com.getjavajob.training.web1702.kashapovv.dao.AbstractDao;
import com.getjavajob.training.web1702.kashapovv.dao.DaoException;
import org.springframework.transaction.annotation.Transactional;

import java.io.InputStream;
import java.util.List;

/**
 * Created by Вадим on 17.07.2017.
 */
public abstract class AbstractService<E extends Entity> implements Service<E> {
    protected AbstractDao<E> dao;

    public AbstractService() {
    }

    @Transactional
    @Override
    public E create(E entity) throws ServiceException, TransactionException {
        try {
            return dao.insert(entity);
        } catch (DaoException e) {
            throw new ServiceException("ERROR - Failed to create " + dao.entityName(), e);
        }
    }

    @Transactional
    @Override
    public void update(E entity) throws ServiceException, TransactionException {
        try {
            dao.update(entity);
        } catch (DaoException e) {
            throw new ServiceException("ERROR - Failed to update " + dao.entityName(), e);
        }
    }

    @Transactional
    @Override
    public void delete(E entity) throws ServiceException, TransactionException {
        try {
            dao.delete(entity);
        } catch (DaoException e) {
            throw new ServiceException("ERROR - Failed to delete " + dao.entityName(), e);
        }
    }

    @Override
    public List<E> getAll() throws ServiceException, TransactionException {
        try {
            return dao.getAll();
        } catch (DaoException e) {
            throw new ServiceException("ERROR - Failed to get all " + dao.entityName() + "s", e);
        }
    }

    @Override
    public E getById(long id) throws ServiceException, TransactionException {
        try {
            return dao.getById(id);
        } catch (DaoException e) {
            throw new ServiceException("ERROR - Failed to find " + dao.entityName() + " by id", e);
        }
    }

    @Transactional
    public void imageUpdate(long id, InputStream is) throws ServiceException, TransactionException {
        try {
            deleteImage(id);
            dao.uploadImage(id, is);
        } catch (DaoException e) {
            throw new ServiceException("ERROR - Failed to upload image " + dao.entityName() + " with id=" + id, e);
        }
    }

    @Transactional
    public byte[] loadImage(long id) throws ServiceException, TransactionException {
        try {
            return dao.loadImage(id);
        } catch (DaoException e) {
            throw new ServiceException("ERROR - Failed to load image " + dao.entityName() + " with id=" + id, e);
        }
    }

    @Transactional
    public void deleteImage(long id) throws ServiceException, TransactionException {
        try {
            dao.deleteImage(id);
        } catch (DaoException e) {
            throw new ServiceException("ERROR - Failed to delete image " + dao.entityName() + " with id=" + id, e);
        }
    }
}
