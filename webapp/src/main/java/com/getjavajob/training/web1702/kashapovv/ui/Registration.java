package com.getjavajob.training.web1702.kashapovv.ui;

import com.getjavajob.training.web1702.kashapovv.model.Account;
import com.getjavajob.training.web1702.kashapovv.model.Phone;
import com.getjavajob.training.web1702.kashapovv.services.AccountService;
import com.getjavajob.training.web1702.kashapovv.services.ServiceException;
import com.getjavajob.training.web1702.kashapovv.services.TransactionException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

import static com.getjavajob.training.web1702.kashapovv.model.Phone.PhoneType.HOME;
import static com.getjavajob.training.web1702.kashapovv.ui.Password.digestPassword;

/**
 * Created by Вадим on 28.06.2017.
 */
@WebServlet("/Registration")
public class Registration extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            AccountService accountService = (AccountService) getServletContext().getAttribute("accountService");
            String email = req.getParameter("email");
            String password = req.getParameter("password");
            //******************************//
            if (email.isEmpty()) {
                req.setAttribute("errorMessage", "Email is empty");
                req.getRequestDispatcher("/registration.jsp").forward(req, resp);
            } else if (password.isEmpty()) {
                req.setAttribute("errorMessage", "Password is empty");
                req.getRequestDispatcher("/registration.jsp").forward(req, resp);
                return;
            }
            //*****************************************//
            if (accountService.findByEmail(email) == null) { // TODO: 11.07.2017 подумать над проверкой данных регистрации
                ArrayList<Phone> personalPhones = new ArrayList<>(); //TODO: можно ли проверять данные сложным regex?
                ArrayList<Phone> jobPhones = new ArrayList<>(); // TODO: 02.02.2018 SSL mysql connection warning

                Arrays.asList(req.getParameterValues("personalPhone")).forEach(
                        (jobPhone) -> personalPhones.add(new Phone(HOME, jobPhone)));
                Arrays.asList(req.getParameterValues("jobPhone")).forEach(
                        (jobPhone) -> jobPhones.add(new Phone(HOME, jobPhone)));

                accountService.createPassword(email, digestPassword(password));
                req.getSession().setAttribute("account", accountService.create(
                        new Account(
                                req.getParameter("lastname"),  //todo отправлять реквест на проверку или созданный объект?
                                req.getParameter("firstname"), //todo как лучше проверять
                                req.getParameter("middlename"),
                                getBirthday(req),
                                personalPhones,
                                jobPhones,
                                req.getParameter("address"),
                                email,
                                req.getParameter("icq"),
                                req.getParameter("skype"),
                                "")));
                req.getRequestDispatcher("/account.jsp").forward(req, resp);
            } else {
                req.setAttribute("errorMessage", "Email already exist");
                req.getRequestDispatcher("/registration.jsp").forward(req, resp);
            }
        } catch (ServiceException | TransactionException e) {
            e.printStackTrace();
        }
    }

    private LocalDate getBirthday(HttpServletRequest req) {
        try {
            return LocalDate.parse(req.getParameter("birthday"));
        } catch (DateTimeException e) {
            return null;
        }
    }
}
