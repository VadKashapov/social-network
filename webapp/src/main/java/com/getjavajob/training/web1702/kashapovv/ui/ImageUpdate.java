package com.getjavajob.training.web1702.kashapovv.ui;

import com.getjavajob.training.web1702.kashapovv.model.Account;
import com.getjavajob.training.web1702.kashapovv.model.Group;
import com.getjavajob.training.web1702.kashapovv.services.AccountService;
import com.getjavajob.training.web1702.kashapovv.services.GroupService;
import com.getjavajob.training.web1702.kashapovv.services.ServiceException;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Вадим on 17.07.2017.
 */
@MultipartConfig
@WebServlet("/imageUpdate")
@Controller
public class ImageUpdate extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        AccountService accountService = (AccountService) getServletContext().getAttribute("accountService");
        GroupService groupService = (GroupService) getServletContext().getAttribute("groupService");
        try {
            if (req.getAttribute("group") != null) {
                Group group = (Group) req.getAttribute("group");
                groupService.imageUpdate(group.getId(), req.getPart("file").getInputStream());
            } else {
                Account account = ((Account) req.getSession().getAttribute("account"));
                accountService.imageUpdate(account.getId(), req.getPart("file").getInputStream());
                resp.sendRedirect(req.getContextPath() + "/account.jsp");
                //// TODO: 18.07.2017 Все эти редиректы - лишние, и заменяется jQuery на стороне пользователя
            }
        } catch (ServiceException e) {
            e.printStackTrace();
        }
    }
}
