package com.getjavajob.training.web1702.kashapovv.ui;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Вадим on 10.07.2017.
 */
public class Password {
    public static byte[] digestPassword(String inputPassword) {
        try {
            return MessageDigest.getInstance("SHA-256").digest(inputPassword.getBytes());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
}
