package com.getjavajob.training.web1702.kashapovv.ui.filters;

import com.getjavajob.training.web1702.kashapovv.services.AccountService;
import com.getjavajob.training.web1702.kashapovv.services.ServiceException;
import com.getjavajob.training.web1702.kashapovv.services.TransactionException;
import com.getjavajob.training.web1702.kashapovv.ui.Password;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Вадим on 13.07.2017.
 */
public class CookieLoginFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        String requestURI = req.getRequestURI();

        if (req.getSession(false) != null && requestURI.equals("/")) {
            resp.sendRedirect(req.getContextPath() + "/account.jsp");
            return;
        }
        if (req.getSession(false) != null || Arrays.asList("/login.jsp", "/login.css", "/Login", "/registration.jsp", "/Registration").contains(requestURI)) {
            chain.doFilter(request, response);
        } else if (req.getSession(false) == null) {
            if (req.getCookies() == null) {
                resp.sendRedirect(req.getContextPath() + "/login.jsp");
                return;
            }
            String email = null;
            String password = null;
            for (Cookie cookie : req.getCookies()) {
                if (cookie.getName().equals("email")) {
                    email = cookie.getValue();
                }
                if (cookie.getName().equals("password")) {
                    password = cookie.getValue();
                }
            }
            AccountService accountService = (AccountService) req.getServletContext().getAttribute("accountService");
            try {
                if (email != null && password != null &&
                        accountService.isAuthorized(email, Password.digestPassword(password))) {
                    req.getSession().setAttribute("account", accountService.findByEmail(email));
                    chain.doFilter(request, response);
                } else {
                    resp.sendRedirect(req.getContextPath() + "/login.jsp");
                }
            } catch (ServiceException | TransactionException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void destroy() {

    }
}