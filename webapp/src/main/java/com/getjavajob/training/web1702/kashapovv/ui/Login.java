package com.getjavajob.training.web1702.kashapovv.ui;

import com.getjavajob.training.web1702.kashapovv.model.Account;
import com.getjavajob.training.web1702.kashapovv.services.AccountService;
import com.getjavajob.training.web1702.kashapovv.services.ServiceException;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.getjavajob.training.web1702.kashapovv.ui.Password.digestPassword;

/**
 * Created by Вадим on 09.07.2017.
 */
@WebServlet("/Login")
@Controller
public class Login extends HttpServlet {
    AccountService accountService;

    @Override
    public void init() throws ServletException {
        accountService = (AccountService) getServletContext().getAttribute("accountService");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        try {
            if (accountService.isAuthorized(email, digestPassword(password))) {
                if (request.getParameter("remember-me") != null) {
                    response.addCookie(new Cookie("email", email));
                    response.addCookie(new Cookie("password", password));
                }
                Account account = accountService.findByEmail(email);
                request.getSession().setAttribute("account", account);
                response.sendRedirect(request.getContextPath() + "/account.jsp?id=" + account.getId());
            } else {
                request.setAttribute("errorMessage", "Username or password is invalid");
                request.getRequestDispatcher("/login.jsp").forward(request, response);
            }
        } catch (ServiceException e) {
            e.printStackTrace();
        }
    }
}
