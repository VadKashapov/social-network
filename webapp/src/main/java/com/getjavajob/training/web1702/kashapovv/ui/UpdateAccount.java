package com.getjavajob.training.web1702.kashapovv.ui;

import com.getjavajob.training.web1702.kashapovv.model.Account;
import com.getjavajob.training.web1702.kashapovv.model.Phone;
import com.getjavajob.training.web1702.kashapovv.services.AccountService;
import com.getjavajob.training.web1702.kashapovv.services.ServiceException;
import com.getjavajob.training.web1702.kashapovv.services.TransactionException;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.ArrayList;

import static com.getjavajob.training.web1702.kashapovv.model.Phone.PhoneType.HOME;
import static com.getjavajob.training.web1702.kashapovv.ui.Password.digestPassword;

/**
 * Created by Вадим on 15.07.2017.
 */
@WebServlet("/UpdateAccount")
@Controller
public class UpdateAccount extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String password = req.getParameter("password");
        Account account = (Account) req.getSession().getAttribute("account");
        try {
            AccountService accountService = (AccountService) getServletContext().getAttribute("accountService");
            if (accountService.isAuthorized(account.getEmail(), digestPassword(password))) {
                ArrayList<Phone> personalPhones = new ArrayList<>();
                ArrayList<Phone> jobPhones = new ArrayList<>();

                for (String personalPhone : req.getParameterValues("personalPhone")) {
                    if (!personalPhone.isEmpty()) {
                        personalPhones.add(new Phone(HOME, personalPhone));
                    }
                }
                for (String jobPhone : req.getParameterValues("jobPhone")) {
                    if (!jobPhone.isEmpty()) {
                        jobPhones.add(new Phone(HOME, jobPhone));
                    }
                }
                Account updatedAccount = new Account(
                        req.getParameter("lastname"),
                        req.getParameter("firstname"),
                        req.getParameter("surname"),
                        getBirthday(req),
                        personalPhones,
                        jobPhones,
                        req.getParameter("address"),
                        account.getEmail(),
                        req.getParameter("icq"),
                        req.getParameter("skype"),
                        "");
                updatedAccount.setId(account.getId());
                accountService.update(updatedAccount);
                if (!req.getParameter("newPassword").isEmpty()) {
                    accountService.updatePassword(account.getEmail(), digestPassword(req.getParameter("newPassword")));
                }
                req.getSession().setAttribute("account", updatedAccount);
                req.getRequestDispatcher("/account.jsp").forward(req, resp);
            } else {
                req.setAttribute("errorPasswordMessage", "Password is wrong");
                req.getRequestDispatcher("/accountUpdate.jsp").forward(req, resp);
            }
        } catch (ServiceException | TransactionException e) {
            e.printStackTrace();
        }
    }

    private LocalDate getBirthday(HttpServletRequest req) {
        try {
            return LocalDate.parse(req.getParameter("birthday"));
        } catch (DateTimeException e) {
            return null;
        }
    }
}
