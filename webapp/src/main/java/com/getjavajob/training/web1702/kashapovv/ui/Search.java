package com.getjavajob.training.web1702.kashapovv.ui;

import com.getjavajob.training.web1702.kashapovv.services.AccountService;
import com.getjavajob.training.web1702.kashapovv.services.GroupService;
import com.getjavajob.training.web1702.kashapovv.services.ServiceException;
import com.getjavajob.training.web1702.kashapovv.services.TransactionException;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Вадим on 15.07.2017.
 */
@WebServlet("/Search")
@Controller
public class Search extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        AccountService accountService = (AccountService) getServletContext().getAttribute("accountService");
        GroupService groupService = (GroupService) getServletContext().getAttribute("groupService");
        String searchValue = req.getParameter("val");
        try {
            req.setAttribute("groups", groupService.findByPartName(searchValue));
            req.setAttribute("accounts", accountService.findByPartName(searchValue));
            req.getRequestDispatcher("/account.jsp").forward(req, resp);
        } catch (ServiceException | TransactionException e) {
            e.printStackTrace();
        }

    }
}
