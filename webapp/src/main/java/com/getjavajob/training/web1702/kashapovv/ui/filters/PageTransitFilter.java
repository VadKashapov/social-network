package com.getjavajob.training.web1702.kashapovv.ui.filters;

import com.getjavajob.training.web1702.kashapovv.model.Account;
import com.getjavajob.training.web1702.kashapovv.model.Group;
import com.getjavajob.training.web1702.kashapovv.services.AccountService;
import com.getjavajob.training.web1702.kashapovv.services.GroupService;
import com.getjavajob.training.web1702.kashapovv.services.ServiceException;
import com.getjavajob.training.web1702.kashapovv.services.TransactionException;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Вадим on 17.07.2017.
 */
public class PageTransitFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        AccountService accountService = (AccountService) request.getServletContext().getAttribute("accountService");
        GroupService groupService = (GroupService) request.getServletContext().getAttribute("groupService");
        HttpServletRequest req = (HttpServletRequest) request;
        String requestURI = req.getRequestURI();
        String requestId = req.getParameter("id");
        String requestAction = req.getParameter("act");

        try {
            switch (requestURI) {
                case "/group.jsp": {
                    Group group = null;
                    if (requestId == null || (group = groupService.getById(Long.parseLong(requestId))) == null) {
                        ((HttpServletResponse) response).sendError(404);
                        break;
                    }
                    req.setAttribute("group", group);
                    if (group.getGroupOwners().contains(req.getSession().getAttribute("account"))) {
                        req.setAttribute("isAdmin", true);
                    }
                    if (requestAction != null && requestAction.equals("edit")) {
                        request.getRequestDispatcher("/groupUpdate.jsp").forward(request, response);
                        return;
                    }
                    chain.doFilter(request, response);
                    break;
                }
                case "/": {
                    long id = ((Account) req.getSession(false).getAttribute("account")).getId();
                    ((HttpServletResponse) response).sendRedirect(req.getContextPath() + "/account.jsp?id=" + id);
                    return;
                }
                case "/account.jsp": {
                    Account account = null;
                    if (requestId == null) {
                        long id = ((Account) req.getSession(false).getAttribute("account")).getId();
                        ((HttpServletResponse) response).sendRedirect(req.getContextPath() + "/account.jsp?id=" + id);
                        return;
                    } else if ((account = accountService.getById(Long.parseLong(requestId))) == null) {
                        ((HttpServletResponse) response).sendError(404);
                        return;
                    }
                    if (requestId.equals(String.valueOf(((Account) req.getSession(false).getAttribute("account")).getId()))) {
                        req.setAttribute("isAdmin", true);
                    }
                    req.setAttribute("account", account);
                    if (requestAction != null && requestAction.equals("edit")) {
                        request.getRequestDispatcher("/accountUpdate.jsp").forward(request, response);
                    }
                    chain.doFilter(request, response);
                    break;
                }
            }
        } catch (ServiceException | TransactionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void destroy() {

    }
}
