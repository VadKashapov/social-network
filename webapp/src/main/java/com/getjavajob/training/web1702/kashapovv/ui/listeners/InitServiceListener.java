package com.getjavajob.training.web1702.kashapovv.ui.listeners;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by Вадим on 03.06.2017.
 */
public class InitServiceListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        WebApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(sce.getServletContext());
        sce.getServletContext().setAttribute("accountService", applicationContext.getBean("accountService"));
        sce.getServletContext().setAttribute("groupService", applicationContext.getBean("groupService"));
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
