package com.getjavajob.training.web1702.kashapovv.ui;

import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Вадим on 15.07.2017.
 */
@WebServlet("/Logout")
@Controller
public class Logout extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().invalidate();
        Arrays.asList(new Cookie("email", ""), new Cookie("password", ""), new Cookie("JSESSIONID", "")).forEach(
                (cookie -> {
                    cookie.setMaxAge(0);
                    cookie.setPath("/");
                    resp.addCookie(cookie);
                })
        );
        resp.sendRedirect(req.getContextPath() + "/login.jsp");
    }
}
