package com.getjavajob.training.web1702.kashapovv.ui;

import com.getjavajob.training.web1702.kashapovv.services.AccountService;
import com.getjavajob.training.web1702.kashapovv.services.GroupService;
import com.getjavajob.training.web1702.kashapovv.services.ServiceException;
import com.getjavajob.training.web1702.kashapovv.services.TransactionException;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Вадим on 18.07.2017.
 */
@WebServlet("/LoadImage")
@Controller
public class LoadImage extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        AccountService accountService = (AccountService) getServletContext().getAttribute("accountService");
        GroupService groupService = (GroupService) getServletContext().getAttribute("groupService");
        String category = req.getParameter("cat");
        long id = Long.parseLong(req.getParameter("id"));
        try {
//            Base64.Encoder encoder = Base64.getEncoder().encode();
//            encoder.encodeToString()
            byte[] image = null;
            switch (category) {
                case "acc":
                    if ((image = accountService.loadImage(id)) == null) {
                        image = IOUtils.toByteArray(getServletContext().getResourceAsStream("images/defaultAccountAvatar.png"));
                    }
                    break;
                case "grp":
                    if ((image = groupService.loadImage(id)) == null) {
                        image = IOUtils.toByteArray(getServletContext().getResourceAsStream("images/defaultGroupAvatar.png"));
                    }
                    break;
            }
            resp.getOutputStream().write(image);
            resp.getOutputStream().flush();
        } catch (ServiceException | TransactionException e) {
            e.printStackTrace();
        }
    }
}
