<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<body>
<jsp:include page="search.jsp"/>
<c:set var="group" value="${requestScope.group}" scope="request"/>
<img src="${pageContext.servletContext.contextPath}/LoadImage?id=${group.id}&cat=grp"/><br>
Name:
${group.name}
Category:
${group.category}
Description:<br>
${group.description}
<br>
Owners:<br>
<table>
    <c:forEach var="admin" items="${group.groupOwners}">
        <tr>
            <td><a href="<c:url value="/account.jsp?id=${admin.id}"/>">Go to</a></td>
            <td>${admin.lastName}</td>
            <td>${admin.firstName}</td>
            <td>${admin.middleName}</td>
            <td>${admin.birthday}</td>
            <td>${admin.address}</td>
        </tr>
    </c:forEach>
</table>
<br>
Subscribers:<br>
<c:if test="${not empty group.groupSubscribers}">
    <table>
        <c:forEach var="subscriber" items="${group.groupSubscribers}">
            <tr>
                <td><a href="<c:url value="/account.jsp?id=${subscriber.id}"/>">Go to</a></td>
                <td>${subscriber.lastName}</td>
                <td>${subscriber.firstName}</td>
                <td>${subscriber.middleName}</td>
                <td>${subscriber.birthday}</td>
                <td>${subscriber.address}</td>
            </tr>
        </c:forEach>
    </table>
</c:if>
<c:if test="${requestScope.isAdmin eq true}">
    <a href="${pageContext.servletContext.contextPath}/group.jsp?id=${group.id}&act=edit" class="btn btn-default">Edit
        group</a>
</c:if>
</body>
</html>
