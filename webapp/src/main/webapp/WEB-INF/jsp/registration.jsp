<%@page session="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>
        var x = 1;
        var max_fields = 10;
        $(document).ready(function () {
            $(".add_field_button").click(function () {
                if (x < max_fields) {
                    x++;
                    $(this).parent('div').append('<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>'); //add input box
                }
                $(".input_fields_wrap").on("click", ".remove_field", function (e) { //user click on remove text
                    x--;
                    e.preventDefault();
                    $(this).parent('div').remove();
                });
            });
        });
    </script>
</head>
<body>
<p style="text-align:center">
    <c:if test="${not empty errorMessage}">
        <c:out value="${errorMessage}"/>
    </c:if>
</p>
<form action="Registration" method="post">
    First name:<br>
    <input type="text" name="firstname"><br>
    Last name: <br>
    <input type="text" name="lastname"><br>
    Middle name:<br>
    <input type="text" name="middlename"><br>
    E-mail:<br>
    <input type="email" name="email"><br>
    Password:<br>
    <input type="password" name="password"><br>
    Birthday:<br>
    <input type="date" name="birthday"><br>
    Address:<br>
    <input type="text" name="address"><br>
    Personal phone:<br>
    <div class="input_fields_wrap">
        <div><input type="tel" name="personalPhone"></div>
        <button class="add_field_button" type="button">Add Phone</button>
    </div>
    Job phone:<br>
    <div class="input_fields_wrap">
        <div><input type="tel" name="jobPhone"></div>
        <button class="add_field_button" type="button">Add Phone</button>
    </div>
    Icq:<br>
    <input type="text" name="icq"><br>
    Skype:<br>
    <input type="text" name="skype"><br>
    <button type="submit">Submit</button>
</form>
</body>
</html>