<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
</head>
<body>
<jsp:include page="search.jsp"/>
<c:set var="account" value="${requestScope.account}" scope="request"/>
<c:if test="${requestScope.isAdmin eq true}">
    <%@include file="uploadImage.jsp" %>
</c:if>
<p>
    <img src="${pageContext.servletContext.contextPath}/LoadImage?id=${account.id}&cat=acc"/><br>
    Last name:
    ${account.lastName}<br>
    First name:
    ${account.firstName}<br>
    Middle name:
    ${account.middleName}<br>
    Birthday:
    ${account.birthday}<br>
    Email:
    ${account.email}<br>
    Personal phones:<br>
    <c:forEach items="${account.getPersonalPhoneNumbers()}" var="phone">
        ${phone.getNumber()}<br>
    </c:forEach>
    Job phones:<br>
    <c:forEach items="${account.getJobPhoneNumbers()}" var="phone">
        ${phone.getNumber()}<br>
    </c:forEach>
    Address:
    ${account.getAddress()}<br>
    ICQ:
    ${account.getIcqId()}<br>
    Skype:
    ${account.getSkypeId()}<br>
</p>
<c:if test="${requestScope.isAdmin eq true}">
    <a href="${pageContext.request.contextPath}/account.jsp?id=${account.id}&act=edit" class="btn">Edit account</a>
</c:if>
</body>
</html>