<%--
  Created by IntelliJ IDEA.
  User: Вадим
  Date: 16.07.2017
  Time: 3:12
  To change this template use File | Settings | File Templates.
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<c:if test="${not empty sessionScope}">
    User:
    <a href="${pageContext.request.contextPath}/account.jsp?id=${sessionScope.account.id}">${sessionScope.account.email}</a><br>
    <a href="${pageContext.request.contextPath}/Logout">Logout</a>
    <form action="${pageContext.request.contextPath}/Search" method="get">
        Search:
        <input type="text" name="val">
    </form>
</c:if>
<jsp:include page="searchResult.jsp"/>
</body>
</html>
