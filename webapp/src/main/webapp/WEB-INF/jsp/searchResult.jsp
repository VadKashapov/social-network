<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Вадим
  Date: 16.07.2017
  Time: 3:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<c:set var="groups" value="${requestScope.groups}" scope="request"/>
<c:set var="accounts" value="${requestScope.accounts}" scope="request"/>
<table>

    <c:forEach items="${groups}" var="group">
        <tr>
            <td><a href="<c:url value="/group.jsp?id=${group.id}"/>">Go to</a></td>
            <td>${group.name}</td>
            <td>${group.category}</td>
            <td>${group.description}</td>
        </tr>
    </c:forEach>
</table>
<table>

    <c:forEach items="${accounts}" var="account">
        <tr>
            <td><a href="<c:url value="/account.jsp?id=${account.id}"/>">Go to</a></td>
            <td>${account.lastName}</td>
            <td>${account.firstName}</td>
            <td>${account.middleName}</td>
            <td>${account.birthday}</td>
            <td>${account.address}</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
