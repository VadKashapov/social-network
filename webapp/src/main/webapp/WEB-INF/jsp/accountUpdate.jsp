<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>
        var x = 1;
        var max_fields = 10;
        $(document).ready(function () {
            $(".add_field_button").click(function () {
                if (x < max_fields) {
                    x++;
                    $(this).parent('div').append('<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>'); //todo не рановато ли этим заниматься?
                }
                $(".input_fields_wrap").on("click", ".remove_field", function (e) { //user click on remove text
                    x--;
                    e.preventDefault();
                    $(this).parent('div').remove();
                });
            });
        });
    </script>
</head>
<body>
<c:set var="account" value="${sessionScope.account}" scope="session"/>
<form action="UpdateAccount" method="post">
    First name:<br>
    <input type="text" name="firstname" value="${account.firstName}"><br>
    Last name: <br>
    <input type="text" name="lastname" value="${account.lastName}"><br>
    Surname:<br>
    <input type="text" name="surname" value="${account.middleName}"><br>
    E-mail:<br>
    <input type="email" name="email" readonly="readonly" value="${account.email}"><br>
    Password:<br>
    <input type="password" name="password">
    <c:if test="${not empty errorPasswordMessage}">
        <c:out value="${errorPasswordMessage}"/>
    </c:if>
    <br>
    New Password:<br>
    <input type="password" name="newPassword"><br>
    Birthday:<br>
    <input type="date" name="birthday" value="${account.birthday}"><br>
    Address:<br>
    <input type="text" name="address" value="${account.address}"><br>
    Personal phone:<br>
    <div class="input_fields_wrap">
        <c:choose>
            <c:when test="${not empty account.getPersonalPhoneNumbers()}">
                <c:forEach items="${account.getPersonalPhoneNumbers()}" var="phone">
                    <div><input type="tel" name="personalPhone" value="${phone.getNumber()}"></div>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <div><input type="tel" name="personalPhone"></div>
            </c:otherwise>
        </c:choose>
        <button class="add_field_button" type="button">Add Phone</button>
    </div>
    Job phone:<br>
    <div class="input_fields_wrap">
        <c:choose>
            <c:when test="${not empty account.getJobPhoneNumbers()}">
                <c:forEach items="${account.getJobPhoneNumbers()}" var="phone">
                    <div><input type="tel" name="jobPhone" value="${phone.getNumber()}"></div>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <div><input type="tel" name="jobPhone"></div>
            </c:otherwise>
        </c:choose>
        <button class="add_field_button" type="button">Add Phone</button>
    </div>
    Icq:<br>
    <input type="text" name="icq" value="${account.icqId}"><br>
    Skype:<br>
    <input type="text" name="skype" value="${account.skypeId}"><br>
    <button type="submit">Submit</button>
</form>
</body>
</html>