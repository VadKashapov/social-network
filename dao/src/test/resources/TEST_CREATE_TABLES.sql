CREATE TABLE accounts
(
    ID INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    LASTNAME VARCHAR(255),
    FIRSTNAME VARCHAR(255),
    MIDDLENAME VARCHAR(255),
    BIRTHDAY DATE,
    ADDRESS VARCHAR(255),
    EMAIL VARCHAR(255),
    ICQID VARCHAR(255),
    SKYPEID VARCHAR(255),
    ADDININFO VARCHAR(10000),
    AVATAR BLOB
);
CREATE UNIQUE INDEX EMAIL_UNIQUE ON accounts (EMAIL);
CREATE TABLE friend_requests
(
    ACC_ID INT(11),
    FRIEND_ID INT(11)
);
CREATE TABLE friends
(
    ACC_ID INT(11),
    FRIEND_ID INT(11)
);
CREATE TABLE group_owners
(
    group_id INT(11) PRIMARY KEY NOT NULL,
    acc_id INT(11) NOT NULL,
    CONSTRAINT fk_group_owners_GROUPS_TBL1 FOREIGN KEY (group_id) REFERENCES groups_tbl (ID)
);
CREATE INDEX fk_group_owners_GROUPS_TBL1_idx ON group_owners (group_id);
CREATE TABLE groups_tbl
(
    ID INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    NAME VARCHAR(255),
    CATEGORY VARCHAR(255),
    DESCRIPTION VARCHAR(10000),
    AVATAR BLOB
);
CREATE UNIQUE INDEX NAME_UNIQUE ON groups_tbl (NAME);
CREATE TABLE images
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    ent_id INT(11),
    type VARCHAR(45) NOT NULL,
    image BLOB
);
CREATE TABLE passwords
(
    email VARCHAR(100) PRIMARY KEY NOT NULL,
    password BINARY(32)
);
CREATE UNIQUE INDEX email_UNIQUE ON passwords (email);
CREATE TABLE phones
(
    PHONE_ID INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    ACC_ID INT(11) NOT NULL,
    TYPE VARCHAR(45),
    NUMBER VARCHAR(45)
);
CREATE TABLE group_subscribers
(
    group_id INT(11) PRIMARY KEY NOT NULL,
    acc_id INT(11) NOT NULL,
    CONSTRAINT fk_participants_GROUPS_TBL1 FOREIGN KEY (group_id) REFERENCES groups_tbl (ID)
);
CREATE INDEX fk_participants_GROUPS_TBL1_idx ON group_subscribers (group_id);