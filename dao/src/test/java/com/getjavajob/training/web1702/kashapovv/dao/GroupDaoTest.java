package com.getjavajob.training.web1702.kashapovv.dao;

import com.getjavajob.training.web1702.kashapovv.Entity;
import com.getjavajob.training.web1702.kashapovv.model.Group;
import com.getjavajob.training.web1702.kashapovv.model.Group.Category;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

/**
 * Created by Вадим on 28.04.2017.
 */
public class GroupDaoTest extends BeforeTestClass {
    private AbstractDao dao;
    private Connection connection;

    @Before
    public void setUp() throws Exception {
        dao = new GroupDao();
        try {
            connection = dataSource.getConnection();
            RunScript.runScript(connection, "TEST_CREATE_TABLES");
            RunScript.runScript(connection, "TEST_INSERT_GROUPS_DATA");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() throws Exception {
        RunScript.runScript(connection, "TEST_DELETE_TABLES");
        connection.close();
    }

    @Test
    public void getEntityById() throws Exception {
        assertEquals(dao.getById(1), new Group("Автомобили", Category.SCIENCE_TECH, "Сообщество автомобилистов"));
    }

    @Test
    public void getAll() throws Exception {
        Collection<Group> actual = dao.getAll();
        Collection<Group> expected = Arrays.asList(
                new Group("Автомобили", Category.valueOf("SCIENCE_TECH"), "Сообщество автомобилистов"),
                new Group("Музыкальные инструменты", Category.valueOf("MUSIC"), "Для тех кто учится или играет на музыкальных инструментах"),
                new Group("Здоровье и красота", Category.valueOf("FASHION_BEAUTY"), "В здоровом теле, здоровый дух"),
                new Group("Популярная механика", Category.valueOf("SCIENCE_TECH"), "Новости науки и техники"),
                new Group("Вокруг света", Category.valueOf("ENTERTAINMENT"), "Путешествия и туризм"),
                new Group("ФК Локомотив", Category.valueOf("SPORT"), "Группа фанатов футбольного клуба Локомотив")
        );
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void create() throws Exception {
        Group group = new Group("Программирование", Category.SCIENCE_TECH, "");
        long id = dao.insert(group).getId();
        assertEquals(group, dao.getById(id));
    }

    @Test
    public void delete() throws Exception {
        Group group = new Group("Программирование", Category.SCIENCE_TECH, "");
        Entity newGroup = dao.insert(group);
        dao.delete(newGroup);
        assertNull(dao.getById(newGroup.getId()));
    }

    @Test
    public void update() throws Exception {
        Group group = new Group("Программирование", Category.SCIENCE_TECH, "");
        long id = dao.insert(group).getId();
        group.setName("Программирование Java");
        dao.update(group);
        assertEquals(group, dao.getById(id));
    }
}