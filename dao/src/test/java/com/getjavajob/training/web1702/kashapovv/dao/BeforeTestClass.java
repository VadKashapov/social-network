package com.getjavajob.training.web1702.kashapovv.dao;

import org.apache.commons.dbcp2.BasicDataSource;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by Вадим on 16.11.2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml","classpath:dao-context-overrides.xml"})
public class BeforeTestClass {
    protected BasicDataSource dataSource;
}

