package com.getjavajob.training.web1702.kashapovv.dao;

import com.getjavajob.training.web1702.kashapovv.model.Account;
import com.getjavajob.training.web1702.kashapovv.model.Phone;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static com.getjavajob.training.web1702.kashapovv.model.Phone.PhoneType.HOME;
import static com.getjavajob.training.web1702.kashapovv.model.Phone.PhoneType.JOB;
import static org.junit.Assert.*;

/**
 * Created by Вадим on 27.04.2017.
 */
public class AccountDaoTest extends BeforeTestClass {
    private AccountDao dao;
    private Connection connection;

    @Before
    public void setUp() throws Exception {
        dao = new AccountDao();
        this.connection = dataSource.getConnection();
        RunScript.runScript(this.connection, "TEST_CREATE_TABLES");
        RunScript.runScript(this.connection, "TEST_INSERT_ACCOUNTS_DATA");
        RunScript.runScript(this.connection, "TEST_INSERT_FRIENDS_DATA");
        RunScript.runScript(this.connection, "TEST_INSERT_PHONES");
    }

    @After
    public void clearUp() throws Exception {
        RunScript.runScript(connection, "TEST_DELETE_TABLES");
        connection.close();
    }

    @Test
    public void getAccountByIdTest() throws Exception {
        assertEquals(new Account("Большаков", "Даниил", "Максимович"
                , LocalDate.of(1990, 3, 2), Arrays.asList(new Phone(HOME, "8(961)125-77-75")), Arrays.asList(new Phone(JOB, "8(495)123-52-43(89)"))
                , "601833, г. Усть-Хайрюзово, ул. Весенняя, дом 85, квартира 121", "bolshak@mail.ru", null, null, null), dao.getById(1));
    }

    @Test(expected = DaoException.class)
    public void insertAlreadyExistAccountsTest() throws Exception {
        Collection<Account> accounts = Arrays.asList(
                new Account("Большаков", "Даниил", "Максимович"
                        , LocalDate.of(1990, 3, 2), Arrays.asList(new Phone(HOME, "8(961)125-77-75")), Arrays.asList(new Phone(JOB, "8(495)123-52-43(89)"))
                        , "601833, г. Усть-Хайрюзово, ул. Весенняя, дом 85, квартира 121", "bolshak@mail.ru", null, null, null),
                new Account("Городнов", "Филимон", "Валентинович", LocalDate.of(1965, 8, 4), Arrays.asList(new Phone(HOME, "8(957)874-49-82"))
                        , new ArrayList<Phone>(), "663282, г. Славинск, ул. Голландская, дом 5, квартира 128", "gorodfilim@mail.ru", null, null, null),
                new Account("Холодкова", "Станислава", "Владиславовна", LocalDate.of(1988, 6, 22), Arrays.asList(new Phone(HOME, "8(948)502-42-34")),
                        new ArrayList<Phone>(), "453661, г. Гагарин, ул. Сержанта Елизарова, дом 90, квартира 101", "holodslava@mail.ru", null, null, null),
                new Account("Чапко", "Елена", "Петровна", LocalDate.of(1985, 4, 21), Arrays.asList(new Phone(HOME, "8(976)898-38-58")),
                        new ArrayList<Phone>(), "346272, г. Ярково, ул. Завокзальная 1-я, дом 89, квартира 153", "chapkolena@mail.ru", null, null, null),
                new Account("Демьянченко", "Федосья", "Сергеевна", LocalDate.of(1968, 7, 13), Arrays.asList(new Phone(HOME, "8(969)390-20-45")),
                        new ArrayList<Phone>(), "641622, г. Поваровка, ул. Байкальская, дом 21, квартира 118", "deminfedosya@mail.ru", null, null, null)
        );
        for (Account account : accounts) {
            dao.insert(account);
        }
    }

    @Test
    public void getAllAccountsTest() throws Exception {
        Collection<Account> expected = Arrays.asList(
                new Account("Большаков", "Даниил", "Максимович"
                        , LocalDate.of(1990, 3, 2), Arrays.asList(new Phone(HOME, "8(961)125-77-75")), Arrays.asList(new Phone(JOB, "8(495)123-52-43(89)"))
                        , "601833, г. Усть-Хайрюзово, ул. Весенняя, дом 85, квартира 121", "bolshak@mail.ru", null, null, null),
                new Account("Городнов", "Филимон", "Валентинович", LocalDate.of(1965, 8, 4), Arrays.asList(new Phone(HOME, "8(957)874-49-82"))
                        , new ArrayList<Phone>(), "663282, г. Славинск, ул. Голландская, дом 5, квартира 128", "gorodfilim@mail.ru", null, null, null),
                new Account("Холодкова", "Станислава", "Владиславовна", LocalDate.of(1988, 6, 22), Arrays.asList(new Phone(HOME, "8(948)502-42-34")),
                        new ArrayList<Phone>(), "453661, г. Гагарин, ул. Сержанта Елизарова, дом 90, квартира 101", "holodslava@mail.ru", null, null, null),
                new Account("Чапко", "Елена", "Петровна", LocalDate.of(1985, 4, 21), Arrays.asList(new Phone(HOME, "8(976)898-38-58")),
                        new ArrayList<Phone>(), "346272, г. Ярково, ул. Завокзальная 1-я, дом 89, квартира 153", "chapkolena@mail.ru", null, null, null),
                new Account("Демьянченко", "Федосья", "Сергеевна", LocalDate.of(1968, 7, 13), Arrays.asList(new Phone(HOME, "8(969)390-20-45")),
                        new ArrayList<Phone>(), "641622, г. Поваровка, ул. Байкальская, дом 21, квартира 118", "deminfedosya@mail.ru", null, null, null)
        );
        assertArrayEquals(expected.toArray(), dao.getAll().toArray());
    }

    @Test
    public void createNewAccountRecordTest() throws Exception {
        Account account = new Account("Борисов", "Арон", "Александрович"
                , LocalDate.of(1991, 12, 11), Arrays.asList(new Phone(HOME, "8(966)134-43-84")), new ArrayList<Phone>()
                , "601833, г. Усть-Хайрюзово, ул. Весенняя, дом 85, квартира 121", "boraron@mail.ru", null, null, null);
        long id = dao.insert(account).getId();
        assertEquals(account, dao.getById(id));
    }

    @Test
    public void deleteAccountTest() throws Exception {
        Account account = new Account("Борисов", "Арон", "Александрович"
                , LocalDate.of(1991, 12, 11), Arrays.asList(new Phone(HOME, "8(966)134-43-84")), new ArrayList<Phone>()
                , "601833, г. Усть-Хайрюзово, ул. Весенняя, дом 85, квартира 121", "boraron@mail.ru", null, null, null);
        Account newAccount = dao.insert(account);
        dao.delete(newAccount);
        assertNull(dao.getById(newAccount.getId()));
    }

    @Test
    public void updateAccountTest() throws Exception {
        Account account = new Account("Борисов", "Арон", "Александрович"
                , LocalDate.of(1991, 12, 11), Arrays.asList(new Phone(HOME, "8(966)134-43-84")), new ArrayList<Phone>()
                , "601833, г. Усть-Хайрюзово, ул. Весенняя, дом 85, квартира 121", "boraron@mail.ru", "", "", "");
        long id = dao.insert(account).getId();
        account.getPersonalPhoneNumbers().get(0).setNumber("8(927)123-12-12");
        dao.update(account);
        assertEquals(account, dao.getById(id));
    }

    @Test
    public void getAllFriendTest() throws Exception {
        assertArrayEquals(new Object[]{dao.getById(1), dao.getById(2), dao.getById(3)}
                , dao.getAllFriends(dao.getById(4)).toArray());
    }

    @Test
    public void addFriendTest() throws Exception {
        Account account = dao.getById(2);
        Account friend = dao.getById(4);
        dao.addFriend(account, friend);
        assertArrayEquals(new Object[]{dao.getById(1), friend}, dao.getAllFriends(account).toArray());
    }

    @Test
    public void checkFriendIsNotExistTest() throws Exception {
        assertFalse(dao.isFriendExist(dao.getById(3), dao.getById(2)));
    }

    @Test
    public void checkFriendIsExistTest() throws Exception {
        assertTrue(dao.isFriendExist(dao.getById(1), dao.getById(2)));
    }

    @Test
    public void removeFriendTest() throws Exception {
        dao.removeFriend(dao.getById(2), dao.getById(1));
        dao.removeFriend(dao.getById(3), dao.getById(1));
        assertEquals(dao.getById(1), dao.getAllFriends(dao.getById(4)).get(0));
    }

    @Test
    public void getPersonalPhoneTest() throws Exception {
        Account account = dao.getById(1);
        assertEquals(account.getPersonalPhoneNumbers(),
                Arrays.asList(new Phone(HOME, "8(961)125-77-75")));
        assertEquals(account.getJobPhoneNumbers(),
                Arrays.asList(new Phone(JOB, "8(495)123-52-43(89)")));
    }
}
