package com.getjavajob.training.web1702.kashapovv;

/**
 * Created by Вадим on 28.04.2017.
 */
public class PoolException extends Exception {

    public PoolException(String message) {
        super(message);
    }

    public PoolException(String message, Throwable cause) {
        super(message, cause);
    }

    protected PoolException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }


}
