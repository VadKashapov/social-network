package com.getjavajob.training.web1702.kashapovv.dao;

import com.getjavajob.training.web1702.kashapovv.model.Message;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Вадим on 15.07.2017.
 */
@Repository
public class MessageDao extends AbstractDao<Message> {
    public MessageDao() throws DaoException {
    }

    @Override
    public String entityName() {
        return "Message";
    }

    @Override
    protected Message createEntityFromResult(ResultSet resultSet) throws SQLException, DaoException {
//        return new Message(resultSet.getString(1),resultSet.getDate(3).toLocalDate()
//                ,resultSet.getLong(2),Message.Purpose.valueOf(resultSet.getString(4)));
        return null;
    }

    @Override
    protected int setAllParamsWithoutId(Message entity, PreparedStatement prep) throws SQLException, DaoException {
        prep.setString(1, entity.getText());
        prep.setLong(2, entity.getAuthor().getId());
        prep.setDate(3, Date.valueOf(entity.getCreated()));
        prep.setString(4, entity.getPurpose().toString());
        prep.setBytes(5, entity.getImage());
        return 6;
    }
}
