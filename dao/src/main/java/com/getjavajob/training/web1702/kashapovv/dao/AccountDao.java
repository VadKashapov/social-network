package com.getjavajob.training.web1702.kashapovv.dao;

import com.getjavajob.training.web1702.kashapovv.model.Account;
import com.getjavajob.training.web1702.kashapovv.model.Phone;
import com.getjavajob.training.web1702.kashapovv.model.Phone.PhoneType;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.getjavajob.training.web1702.kashapovv.Dao.SqlOperation.*;

/**
 * Created by Вадим on 26.04.2017.
 */
@Repository
public class AccountDao extends AbstractDao<Account> {
    {
        sqlProperties = new HashMap<>();
        sqlProperties.put(SELECT_BY_ID, "SELECT_BY_ID_ACCOUNTS");
        sqlProperties.put(SELECT_ALL, "SELECT_ALL_ACCOUNTS");
        sqlProperties.put(INSERT_INTO, "INSERT_INTO_ACCOUNTS");
        sqlProperties.put(UPDATE_BY_ID, "UPDATE_BY_ID_ACCOUNTS");
        sqlProperties.put(DELETE_BY_ID, "DELETE_BY_ID_ACCOUNTS");
        sqlProperties.put(SELECT_BY_PARTNAME, "SELECT_ACCOUNTS_BY_PARTNAME");
    }

    public AccountDao() throws DaoException {
    }

    @Override
    public String entityName() {
        return "Account";
    }

    @Override
    public Account createEntityFromResult(ResultSet resultSet) throws SQLException, DaoException {
        List<Phone> personalPhoneNumbers = new ArrayList<>();
        List<Phone> jobPhoneNumbers = new ArrayList<>();
        for (Phone phone : getAccountPhones(resultSet.getLong("id"))) {
            if (phone.getType() == PhoneType.HOME) {
                personalPhoneNumbers.add(phone);
            } else if (phone.getType() == PhoneType.JOB) {
                jobPhoneNumbers.add(phone);
            }
        }
        Account newAccount = new Account(
                resultSet.getString("lastName"),
                resultSet.getString("firstName"),
                resultSet.getString("middleName"),
                resultSet.getDate("birthday") == null ? null : resultSet.getDate("birthday").toLocalDate(),
                personalPhoneNumbers,
                jobPhoneNumbers,
                resultSet.getString("address"),
                resultSet.getString("email"),
                resultSet.getString("icqId"),
                resultSet.getString("skypeId"),
                resultSet.getString("addInInfo"));
        newAccount.setId(resultSet.getLong("id"));
        newAccount.setFriends(getAllFriendsOnce(resultSet.getLong("id")));
        return newAccount;
    }

    @Override
    protected int setAllParamsWithoutId(Account account, PreparedStatement prepStatement) throws SQLException, DaoException {
        prepStatement.setString(1, account.getLastName());
        prepStatement.setString(2, account.getFirstName());
        prepStatement.setString(3, account.getMiddleName());
        prepStatement.setDate(4, account.getBirthday() == null ? null : Date.valueOf(account.getBirthday()));
        prepStatement.setString(5, account.getAddress());
        prepStatement.setString(6, account.getEmail());
        prepStatement.setString(7, account.getIcqId());
        prepStatement.setString(8, account.getSkypeId());
        prepStatement.setString(9, account.getAddInInfo());
        return 9;
    }

    @Override
    public void delete(Account entity) throws DaoException {
        super.delete(entity);
        deleteAllFriends(entity);
        deletePhones(entity.getId());
    }

    @Override
    public void update(Account entity) throws DaoException {
        super.update(entity);
        deletePhones(entity.getId());
        insertPhones(entity);
    }

    @Override
    public Account insert(Account entity) throws DaoException {
        Account account = super.insert(entity);
        insertPhones(entity);
        return account;
    }

    //*******************************************Load friends in 1 level deep *********************************************/
    public List<Account> getAllFriendsOnce(long id) throws DaoException {
        return this.jdbcTemplate.query(getSql("SELECT_ALL_FRIENDS_BY_ACC_ID"), resultSet -> {
            List<Account> friends = new ArrayList<>();
            while (resultSet.next()) {
                friends.add(getByIdWithoutFriends(resultSet.getInt(1)));
            }
            return friends;
        }, id, id);
    }

    private Account getByIdWithoutFriends(int id) throws DaoException {
        return this.jdbcTemplate.queryForObject(getSql(getFullPropertyName(SELECT_BY_ID)), (resultSet, i) -> {
            return createEntityFromResultWithoutFriends(resultSet);
        }, id);
    }

    public Account createEntityFromResultWithoutFriends(ResultSet resultSet) throws SQLException, DaoException {
        List<Phone> personalPhoneNumbers = new ArrayList<>();
        List<Phone> jobPhoneNumbers = new ArrayList<>();
        for (Phone phone : getAccountPhones(resultSet.getLong("id"))) {
            if (phone.getType() == PhoneType.HOME) {
                personalPhoneNumbers.add(phone);
            } else if (phone.getType() == PhoneType.JOB) {
                jobPhoneNumbers.add(phone);
            }
        }
        Account newAccount = new Account(
                resultSet.getString("lastName"),
                resultSet.getString("firstName"),
                resultSet.getString("middleName"),
                resultSet.getDate("birthday").toLocalDate(),
                personalPhoneNumbers,
                jobPhoneNumbers,
                resultSet.getString("address"),
                resultSet.getString("email"),
                resultSet.getString("icqId"),
                resultSet.getString("skypeId"),
                resultSet.getString("addInInfo"));
        newAccount.setId(resultSet.getInt("id"));
        return newAccount;
    }

    //*********************************************Password CRUD *********************************************************/
    public boolean isAuthorized(String email, byte[] password) throws DaoException {
        return jdbcTemplate.query(getSql("CHECK_PASSWORD"), ResultSet::next, email, password);
    }

    public void insertPassword(String email, byte[] password) throws DaoException {
        jdbcTemplate.update(getSql("INSERT_PASSWORD"), email, password);
    }

    public void updatePassword(String email, byte[] password) throws DaoException {
        jdbcTemplate.update(getSql("UPDATE_PASSWORD"), password, email);
    }

    //*************************************************Search ********************************************************//
    public Account getByEmail(String email) throws DaoException {
        return this.jdbcTemplate.queryForObject(getSql("SELECT_BY_EMAIL"), (resultSet, i) -> {
                return createEntityFromResult(resultSet);
        }, email);
    }

    public List<Account> findByPartName(String partName) throws DaoException {
        return this.jdbcTemplate.query(getSql("SELECT_ACCOUNTS_BY_PARTNAME"), resultSet -> {
            List<Account> entities = new ArrayList<>();
            while (resultSet.next()) {
                entities.add(createEntityFromResult(resultSet));
            }
            return entities;
        }, partName, partName);
    }

    //******************************************* Phone CRUD**************************************************************//

    public void insertPhones(Account account) throws DaoException {
        long id = account.getId();
        this.jdbcTemplate.batchUpdate(getSql("INSERT_PHONES"), account.getPersonalPhoneNumbers()
                , account.getPersonalPhoneNumbers().size(), (prepStatement, phone) -> {
                    prepStatement.setLong(1, id);
                    prepStatement.setString(2, phone.getType().name());
                    prepStatement.setString(3, phone.getNumber());
                });
        this.jdbcTemplate.batchUpdate(getSql("INSERT_PHONES"), account.getJobPhoneNumbers()
                , account.getJobPhoneNumbers().size(), (prepStatement, phone) -> {
                    prepStatement.setLong(1, id);
                    prepStatement.setString(2, phone.getType().name());
                    prepStatement.setString(3, phone.getNumber());
                });
    }

    public void deletePhones(long id) throws DaoException {
        this.jdbcTemplate.update(getSql("DELETE_PHONES"), id);
    }

    public List<Phone> getAccountPhones(long id) {
        return this.jdbcTemplate.query(getSql("GET_ACCOUNT_PHONES"), resultSet -> {
            List<Phone> accountPhones = new ArrayList<>();
            while (resultSet.next()) {
                accountPhones.add((createPhone(resultSet)));
            }
            return accountPhones;
        }, id);
    }

    private Phone createPhone(ResultSet resultSet) throws DaoException {
        try {
            return new Phone(PhoneType.valueOf(resultSet.getString("type")), resultSet.getString("number"));
        } catch (SQLException | NullPointerException e) {
            throw new DaoException("ERROR: phone can't be created");
        }
    }

    //*********************************************Friends CRUD********************************************************/
    private void checkForDuplicate(Account account, Account friend) throws DaoException {
        if (account.getId() == friend.getId()) {
            throw new DaoException("ERROR - Found a try account to add friend himself");
        }
    }

    public boolean addFriendRequest(Account account, Account friend) throws DaoException {
        checkForDuplicate(account, friend);
        jdbcTemplate.update(getSql("INSERT_FRIEND_REQUEST"), account.getId(), friend.getId());
        return true;
    }

    public boolean removeFriendRequest(Account account, Account friend) throws DaoException {
        checkForDuplicate(account, friend);
        jdbcTemplate.update(getSql("DELETE_FRIEND_REQUEST"), account.getId(), friend.getId());
        return true;
    }

    public boolean isFriendRequestApplied(Account account, Account friend) throws DaoException {
        checkForDuplicate(account, friend);
        return jdbcTemplate.query(getSql("SELECT_CHECK_APPLY_FRIEND_REQUEST"), ResultSet::next, account.getId(), friend.getId());
    }

    public boolean addFriend(Account account, Account friend) throws DaoException {
        checkForDuplicate(account, friend);
        jdbcTemplate.update(getSql("INSERT_FRIEND"), account.getId(), friend.getId());
        return true;
    }

    public boolean isFriendExist(Account account, Account friend) throws DaoException {
        checkForDuplicate(account, friend);
        return jdbcTemplate.query(getSql("SELECT_CHECK_IF_FRIEND_EXIST"), ResultSet::next
                , account.getId(), friend.getId(), friend.getId(), account.getId());
    }

    public boolean removeFriend(Account account, Account friend) throws DaoException {
        checkForDuplicate(account, friend);
        jdbcTemplate.update(getSql("DELETE_ACCOUNT_FROM_FRIENDS"), account.getId(), friend.getId(), friend.getId(), account.getId());
        return true;
    }

    public List<Account> getAllFriends(Account account) throws DaoException {
        return jdbcTemplate.query(getSql("SELECT_ALL_FRIENDS_BY_ACC_ID"), resultSet -> {
            List<Account> friends = new ArrayList<>();
            while (resultSet.next()) {
                friends.add(getById(resultSet.getInt(1)));
            }
            return friends;
        }, account.getId(), account.getId());
    }

    private void deleteAllFriends(Account account) throws DaoException {
        jdbcTemplate.update(getSql("DELETE_ALL_FRIENDS"), account.getId(), account.getId());
    }
}