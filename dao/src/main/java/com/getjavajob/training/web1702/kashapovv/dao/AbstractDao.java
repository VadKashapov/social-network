package com.getjavajob.training.web1702.kashapovv.dao;

import com.getjavajob.training.web1702.kashapovv.Dao;
import com.getjavajob.training.web1702.kashapovv.Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static com.getjavajob.training.web1702.kashapovv.Dao.SqlOperation.*;

/**
 * Created by Вадим on 26.04.2017.
 */
public abstract class AbstractDao<E extends Entity> implements Dao<E> {
    public static final String SQL_STATEMENTS_PROPERTIES = "sqlStatements.properties";

    protected Properties sqlStats;
    protected Map<SqlOperation, String> sqlProperties;
    @Autowired
    protected JdbcTemplate jdbcTemplate; //todo Попытаться инициализировать через сеттер Спринг

    public AbstractDao() throws DaoException {
        try {
            sqlStats = new Properties();
            sqlStats.load(getResource(SQL_STATEMENTS_PROPERTIES));
        } catch (IOException e) {
            throw new DaoException("ERROR - Failed to load " + SQL_STATEMENTS_PROPERTIES, e);
        }
    }

    private InputStream getResource(String resource) throws DaoException {
        InputStream in = getClass().getClassLoader().getResourceAsStream(resource);
        if (in == null) {
            throw new DaoException("ERROR - Resource " + resource + " not found");
        }
        return in;
    }

    @Override
    public E getById(long id) throws DaoException {
        return this.jdbcTemplate.queryForObject(getSql(getFullPropertyName(SELECT_BY_ID)), (resultSet, i) -> {
            return createEntityFromResult(resultSet);
        }, id);
    }

    @Override
    public List<E> getAll() throws DaoException {
        return this.jdbcTemplate.query(getSql(getFullPropertyName(SELECT_ALL)), resultSet -> {
            List<E> entities = new ArrayList<>();
            while (resultSet.next()) {
                entities.add(createEntityFromResult(resultSet));
            }
            return entities;
        });
    }

    @Transactional
    @Override
    public E insert(E entity) throws DaoException {
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        this.jdbcTemplate.update(connection -> {
            PreparedStatement prepStatement = connection.prepareStatement(getSql(getFullPropertyName(INSERT_INTO))
                    , Statement.RETURN_GENERATED_KEYS);
            setAllParamsWithoutId(entity, prepStatement);
            return prepStatement;
        }, keyHolder);

        entity.setId(keyHolder.getKey().longValue());
        return entity;
    }

    @Override
    public void delete(E entity) throws DaoException {
        this.jdbcTemplate.update(getSql(getFullPropertyName(DELETE_BY_ID)), prepStatement -> {
            prepStatement.setLong(1, entity.getId());
        });
    }

    @Override
    public void update(E entity) throws DaoException {
        this.jdbcTemplate.update(getSql(getFullPropertyName(UPDATE_BY_ID)), prepStatement -> {
            int lastParamIndex = setAllParamsWithoutId(entity, prepStatement);
            prepStatement.setLong(lastParamIndex + 1, entity.getId());
        });
    }

    protected String getSql(String property) throws DaoException {
        String sql = sqlStats.getProperty(property);
        if (sql == null) {
            throw new DaoException(property + " not found in " + SQL_STATEMENTS_PROPERTIES);
        }
        return sql;
    }

    protected String getFullPropertyName(SqlOperation type) throws DaoException {
        String property = sqlProperties.get(type);
        if (property == null) {
            throw new DaoException("Property " + type.name() + " not found in entity statement mapper");
        }
        return property;
    }

    public void uploadImage(long id, InputStream is) throws DaoException {
        this.jdbcTemplate.update(getSql("UPLOAD_IMAGE"), prepStatement -> {
            prepStatement.setLong(1, id);
            prepStatement.setBlob(2, is);
            prepStatement.setString(3, entityName());
        });
    }

    public byte[] loadImage(long id) throws DaoException {
        return jdbcTemplate.query(getSql("LOAD_IMAGE"), resultSet -> {
            resultSet.next();
            return resultSet.getBytes(1);
        }, id, entityName());
    }

    public void deleteImage(long id) throws DaoException {
        jdbcTemplate.update(getSql("DELETE_IMAGE"), prepStatement -> {
            prepStatement.setLong(1, id);
        });
    }

    public abstract String entityName();

    protected abstract E createEntityFromResult(ResultSet resultSet) throws SQLException, DaoException;

    protected abstract int setAllParamsWithoutId(E entity, PreparedStatement prep) throws SQLException;
}
