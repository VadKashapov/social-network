package com.getjavajob.training.web1702.kashapovv.dao;

import com.getjavajob.training.web1702.kashapovv.model.Account;
import com.getjavajob.training.web1702.kashapovv.model.Group;
import com.getjavajob.training.web1702.kashapovv.model.Group.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static com.getjavajob.training.web1702.kashapovv.Dao.SqlOperation.*;

/**
 * Created by Вадим on 26.04.2017.
 */
@Repository
public class GroupDao extends AbstractDao<Group> {
    @Autowired
    private AccountDao accountDao;

    {
        sqlProperties = new HashMap<>();
        sqlProperties.put(SELECT_BY_ID, "SELECT_BY_ID_GROUPS_TBL");
        sqlProperties.put(SELECT_ALL, "SELECT_ALL_GROUPS_TBL");
        sqlProperties.put(INSERT_INTO, "INSERT_INTO_GROUPS_TBL");
        sqlProperties.put(UPDATE_BY_ID, "UPDATE_BY_ID_GROUPS_TBL");
        sqlProperties.put(DELETE_BY_ID, "DELETE_BY_ID_GROUPS_TBL");
        sqlProperties.put(SELECT_BY_PARTNAME, "SELECT_GROUPS_BY_PARTNAME");
    }

    public GroupDao() throws DaoException {
    }

    @Override
    public Group getById(long id) throws DaoException {
        Group group = super.getById(id);
        group.setGroupOwners(getOwners(id));
        group.setGroupSubscribers(getSubscribers(id));
        return group;
    }

    @Override
    public String entityName() {
        return "Group";
    }

    @Override
    protected Group createEntityFromResult(ResultSet resultSet) throws SQLException {
        Group newGroup = new Group(resultSet.getString("NAME"),
                Category.valueOf(resultSet.getString("CATEGORY")), resultSet.getString("DESCRIPTION")
        );
        newGroup.setId(resultSet.getLong("id"));
        return newGroup;
    }

    @Override
    protected int setAllParamsWithoutId(Group group, PreparedStatement prepStatement) throws SQLException {
        prepStatement.setString(1, group.getName());
        prepStatement.setString(2, group.getCategory().name());
        prepStatement.setString(3, group.getDescription());
        return 3;
    }

    public List<Group> findByPartName(String partName) throws DaoException {
        return jdbcTemplate.query(getSql("SELECT_GROUPS_BY_PARTNAME"), resultSet -> {
            List<Group> entities = new ArrayList<>();
            while (resultSet.next()) {
                entities.add(createEntityFromResult(resultSet));
            }
            return entities;
        }, partName);
    }

    public Set<Account> getOwners(long id) throws DaoException {
        return jdbcTemplate.query(getSql("SELECT_GROUP_OWNERS"), resultSet -> {
            Set<Account> entities = new HashSet<>();
            while (resultSet.next()) {
                entities.add(accountDao.getById(resultSet.getLong(1)));
            }
            return entities;
        }, id);
    }

    public Set<Account> getSubscribers(long id) throws DaoException {
        return jdbcTemplate.query(getSql("SELECT_GROUP_SUBSCRIBERS"), resultSet -> {
            Set<Account> entities = new HashSet<>();
            while (resultSet.next()) {
                entities.add(accountDao.getById(resultSet.getLong(1)));
            }
            return entities;
        }, id);
    }
}
