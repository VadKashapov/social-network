package com.getjavajob.training.web1702.kashapovv;

import com.getjavajob.training.web1702.kashapovv.dao.DaoException;

import java.util.List;

/**
 * Created by Вадим on 28.04.2017.
 */
public interface Dao<E extends Entity> {
    E getById(long id) throws DaoException, PoolException;

    List<E> getAll() throws DaoException;

    E insert(E entity) throws DaoException;

    void delete(E account) throws DaoException;

    void update(E entity) throws DaoException;

    enum SqlOperation {
        SELECT_BY_ID, SELECT_ALL, INSERT_INTO, UPDATE_BY_ID, DELETE_BY_ID, SELECT_BY_PARTNAME,
    }
}
